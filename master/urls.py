from django.conf.urls import url
from master.views import *
#from rest_framework.routers import DefaultRouter
from rest_framework import routers
router = routers.DefaultRouter()

router.register(r'autor', AutorViewSet)
router.register(r'book', BookViewSet)

urlpatterns = router.urls