from rest_framework import serializers
from master.models import *

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = book
        fields = ['id','fkautor','title']
class AutorSerializer(serializers.ModelSerializer):
    books = BookSerializer(many=True, read_only=True)
    class Meta:
        model = autor
        fields = ['id','name','books']