from rest_framework.viewsets import ModelViewSet
from django.shortcuts import render
from master.serializers import *

# Create your views here.
class AutorViewSet(ModelViewSet):
    queryset = autor.objects.all()
    serializer_class = AutorSerializer

class BookViewSet(ModelViewSet):
    queryset = book.objects.all()
    serializer_class = BookSerializer