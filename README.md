**Dependencia**
*  python3 : https://www.python.org/download/releases/3.0/
*  pip : https://pip.pypa.io/en/stable/installing/
*  virtualenv : https://virtualenv.pypa.io/en/stable/installation.html

**Instalaciòn**
1.  crear virtualenv: virtualenv testnetzum --python=python3
2.  dirigirse al directorio testnetzum, ejecutar el siguiente comando: source bin/activate
3.  en el directorio testnetzum clonar repositorio: git clone git@gitlab.com:oscarterce/testnetzum.git
4.  instalar dependencias: ir al directorio de la clonación e instalar dependencias #pip install -r requirements.txt
5.  iniciar project: python manage.py runserver

**Demo**
https://recordit.co/2u9PeUuAHL